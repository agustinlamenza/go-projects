package main

import (
	"fmt"
)

func main() {
	m := Man{name: "agustin", lastName: "gonzalez"}

	test(m)
}

// Man is a type
type Man struct {
	name     string
	lastName string
}

func (m Man) show() {
	fmt.Println(m.name)
}

func (m Man) print() {
	fmt.Println(m.lastName)
}

// I is an interface
type I interface {
	show()
	print()
}

func test(o I) {
	o.print()
	o.show()
	fmt.Println(o)
}
