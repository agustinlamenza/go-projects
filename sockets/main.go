package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"strings"
)

//Hub is the client manager for connections
type Hub struct {
	clients    map[*Client]bool
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
}

//Client represent a TCP connection to one client
type Client struct {
	socket net.Conn
	data   chan []byte
}

//Start the connection manager
func (h *Hub) start() {
	for {
		select {
		case connection := <-h.register:
			h.clients[connection] = true
		case connection := <-h.unregister:
			close(connection.data)
			delete(h.clients, connection)
		case message := <-h.broadcast:
			for conn := range h.clients {
				conn.data <- message
			}
		}
	}
}

//The server receives messages
func (h *Hub) receive(c *Client) {
	for {
		message := make([]byte, 4096)
		length, _ := c.socket.Read(message)
		if length > 0 {
			fmt.Println("Received: ", string(message))
			h.broadcast <- message
		}
	}
}

//The server send messages to client
func (h *Hub) send(c *Client) {
	defer c.socket.Close()
	for {
		message, _ := <-c.data
		c.socket.Write(message)
	}
}

func (c *Client) receive() {
	for {
		message := make([]byte, 4096)
		length, _ := c.socket.Read(message)
		if length > 0 {
			fmt.Println("Received: ", string(message))
		}
	}
}

func startServer() {
	fmt.Println("Starts server")
	listener, err := net.Listen("tcp", ":3000")
	if err != nil {
		fmt.Println(err)
	}

	hub := Hub{
		clients:    make(map[*Client]bool),
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}

	go hub.start()

	for {
		connection, _ := listener.Accept()
		client := &Client{
			socket: connection,
			data:   make(chan []byte),
		}
		hub.register <- client
		go hub.receive(client)
		go hub.send(client)
	}
}

func startClient() {
	fmt.Println("Starts a client")
	connection, _ := net.Dial("tcp", "localhost:3000")
	client := Client{
		socket: connection,
	}
	go client.receive()

	for {
		reader := bufio.NewReader(os.Stdin)
		message, _ := reader.ReadString('\n')
		connection.Write([]byte(strings.TrimRight(message, "\n")))
	}
}

func main() {
	flagMode := flag.String("mode", "server", "start as a server or client")

	flag.Parse()

	if strings.ToLower(*flagMode) == "server" {
		startServer()
	} else {
		startClient()
	}
}
