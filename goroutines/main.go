package main

import (
	"fmt"
	"time"
)

func main() {
	c1 := make(chan string, 10000)
	c2 := make(chan string, 20000)
	c3 := make(chan string, 30000)

	go startProd(c1)
	go startProd(c2)
	go startProd(c3)

	for {
		select {
		case str := <-c1:
			fmt.Println("C1 Consumed: ", str)
		case str := <-c2:
			fmt.Println("C2 Consumed: ", str)
		case str := <-c3:
			fmt.Println("C3 Consumed: ", str)
		}
		time.Sleep(200 * time.Millisecond)
	}
}

func startProd(c chan string) {
	for {
		str := time.Now().String()
		c <- str
		fmt.Println("Produced: ", str)
		time.Sleep(1 * time.Millisecond)
	}
}
